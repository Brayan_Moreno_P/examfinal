using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using Clidad.Repository;
using Clidad.Controllers;
using Clidad.Models;
using System.Collections.Generic;
using System;

namespace CalidadTest
{
    public class Tests
    {
        [Test]
        public void MuestraPantallaPrincipal()
        {
            var app = new Mock<IHomeRepository>();
            var controller = new HomeController(app.Object);

            var index = controller.Index();
            Assert.IsInstanceOf<ViewResult>(index);
        }



        [Test]
        public void GeneraAleatorio()
        {
            var app = new Mock<IHomeRepository>();
            app.Setup(o => o.GenerarCartas()).Returns(new List<Carta>());

            var controller = new HomeController(app.Object);
            var index = controller.Index();
            Assert.IsInstanceOf<ViewResult>(index);
        }



        [Test]
        public void CartasDiferentes()
        {
            var app = new Mock<IHomeRepository>();

            if (app.Setup(o => o.GenerarCartas()).Returns(new List<Carta>()) == null)
            {
                app.Setup(o => o.GenerarCartas()).Returns(new List<Carta>());
                var controller = new HomeController(app.Object);
                var index = controller.Index();
                Assert.IsInstanceOf<ViewResult>(index);
            }
        }



        [Test]
        public void EscaleraDeColor()
        {
            List<Carta> cartas = new List<Carta>();
            cartas.Add(new Carta() { numero = 5, tipo = 2 });
            cartas.Add(new Carta() { numero = 6, tipo = 2 });
            cartas.Add(new Carta() { numero = 7, tipo = 2 });
            cartas.Add(new Carta() { numero = 8, tipo = 2 });
            cartas.Add(new Carta() { numero = 9, tipo = 2 });

            var app = new Mock<IHomeRepository>();
            var verdad = 0;
            var result = app.Setup(o => o.EscaleraDeColor(cartas)).Returns(verdad);
            Assert.AreNotEqual(result, 1);
        }



        [Test]
        public void EscaleraReal()
        {
            List<Carta> cartas = new List<Carta>();
            cartas.Add(new Carta() { numero = 1, tipo = 1 });
            cartas.Add(new Carta() { numero = 10, tipo = 1 });
            cartas.Add(new Carta() { numero = 11, tipo = 1 });
            cartas.Add(new Carta() { numero = 12, tipo = 1 });
            cartas.Add(new Carta() { numero = 13, tipo = 1 });

            var app = new Mock<IHomeRepository>();
            var result = app.Setup(o => o.EscaleraDeColor(cartas)).Returns(0);
            Assert.AreNotEqual(result, 1);
        }



        [Test]
        public void Poker()
        {
            List<Carta> cartas = new List<Carta>();
            cartas.Add(new Carta() { numero = 7, tipo = 4 });
            cartas.Add(new Carta() { numero = 1, tipo = 4 });
            cartas.Add(new Carta() { numero = 1, tipo = 3 });
            cartas.Add(new Carta() { numero = 1, tipo = 2 });
            cartas.Add(new Carta() { numero = 1, tipo = 1 });
            cartas.Add(new Carta() { numero = 1, tipo = 0 });

            var app = new Mock<IHomeRepository>();
            var result = app.Setup(o => o.Poker(cartas)).Returns(0);
            Assert.AreNotEqual(result, 0);
        }



        [Test]
        public void Full()
        {
            List<Carta> cartas = new List<Carta>();
            cartas.Add(new Carta() { numero = 10, tipo = 4 });
            cartas.Add(new Carta() { numero = 10, tipo = 3 });
            cartas.Add(new Carta() { numero = 13, tipo = 2 });
            cartas.Add(new Carta() { numero = 13, tipo = 1 });
            cartas.Add(new Carta() { numero = 13, tipo = 0 });

            var app = new Mock<IHomeRepository>();
            var result = app.Setup(o => o.Poker(cartas)).Returns(0);
            Assert.IsNotNull(result);
        }


        [Test]
        public void Color()
        {
            List<Carta> cartas = new List<Carta>();
            cartas.Add(new Carta() { numero = 12, tipo = 1 });
            cartas.Add(new Carta() { numero = 10, tipo = 1 });
            cartas.Add(new Carta() { numero = 9, tipo = 1 });
            cartas.Add(new Carta() { numero = 7, tipo = 1 });
            cartas.Add(new Carta() { numero = 4, tipo = 1 });

            var app = new Mock<IHomeRepository>();
            var result = app.Setup(o => o.Color(cartas)).Returns(0);
            Assert.AreNotEqual(result, null);
        }



        [Test]
        public void EscaleraNormal()
        {
            List<Carta> cartas = new List<Carta>();
            cartas.Add(new Carta() { numero = 2, tipo = 0 });
            cartas.Add(new Carta() { numero = 4, tipo = 1 });
            cartas.Add(new Carta() { numero = 5, tipo = 2 });
            cartas.Add(new Carta() { numero = 6, tipo = 3 });
            cartas.Add(new Carta() { numero = 7, tipo = 2 });

            var app = new Mock<IHomeRepository>();
            var ordenar = app.Setup(o => o.Ordenar(cartas)).Returns(new List<Carta>());
            var result = app.Setup(o => o.Escalera(cartas)).Returns(0);
            Assert.IsNotEmpty(cartas);
        }

        [Test]
        public void Trio()
        {
            List<Carta> cartas = new List<Carta>();
            cartas.Add(new Carta() { numero = 2, tipo = 0 });
            cartas.Add(new Carta() { numero = 3, tipo = 1 });
            cartas.Add(new Carta() { numero = 10, tipo = 1 });
            cartas.Add(new Carta() { numero = 10, tipo = 2 });
            cartas.Add(new Carta() { numero = 10, tipo = 3 });

            var app = new Mock<IHomeRepository>();
            var ordenar = app.Setup(o => o.Ordenar(cartas)).Returns(new List<Carta>());
            var result = app.Setup(o => o.Trio(cartas)).Returns(0);
            Assert.IsNotEmpty(cartas);
        }


        [Test]
        public void DoublePar()
        {
            List<Carta> cartas = new List<Carta>();
            cartas.Add(new Carta() { numero = 2, tipo = 0 });
            cartas.Add(new Carta() { numero = 3, tipo = 1 });
            cartas.Add(new Carta() { numero = 10, tipo = 1 });
            cartas.Add(new Carta() { numero = 10, tipo = 2 });
            cartas.Add(new Carta() { numero = 10, tipo = 3 });

            var app = new Mock<IHomeRepository>();
            var ordenar = app.Setup(o => o.Ordenar(cartas)).Returns(new List<Carta>());
            var result = app.Setup(o => o.DoblePar(cartas)).Returns(0);
            Assert.AreNotEqual(cartas, result);
        }


        [Test]
        public void UnPar()
        {
            List<Carta> cartas = new List<Carta>();
            cartas.Add(new Carta() { numero = 2, tipo = 0 });
            cartas.Add(new Carta() { numero = 3, tipo = 1 });
            cartas.Add(new Carta() { numero = 10, tipo = 1 });
            cartas.Add(new Carta() { numero = 10, tipo = 2 });
            cartas.Add(new Carta() { numero = 10, tipo = 3 });

            var app = new Mock<IHomeRepository>();
            var ordenar = app.Setup(o => o.Ordenar(cartas)).Returns(new List<Carta>());
            var result = app.Setup(o => o.DoblePar(cartas)).Returns(0);
            Assert.IsNotNull(result);
        }


        
        [Test]
        public void CartaAlta()
        {
            List<Carta> cartas = new List<Carta>();
            cartas.Add(new Carta() { numero = 2, tipo = 0 });
            cartas.Add(new Carta() { numero = 3, tipo = 1 });
            cartas.Add(new Carta() { numero = 10, tipo = 1 });
            cartas.Add(new Carta() { numero = 10, tipo = 2 });
            cartas.Add(new Carta() { numero = 10, tipo = 3 });

            var app = new Mock<IHomeRepository>();
            var ordenar = app.Setup(o => o.Ordenar(cartas)).Returns(new List<Carta>());
            var result = app.Setup(o => o.DoblePar(cartas)).Returns(0);
            Assert.AreNotEqual(result, 0);
        }


        [Test]
        public void Ganador()
        {
            List<Carta> cartas = new List<Carta>();
            cartas.Add(new Carta() { numero = 2, tipo = 0 });
            cartas.Add(new Carta() { numero = 3, tipo = 1 });
            cartas.Add(new Carta() { numero = 10, tipo = 1 });
            cartas.Add(new Carta() { numero = 10, tipo = 2 });
            cartas.Add(new Carta() { numero = 10, tipo = 3 });

            var app = new Mock<IHomeRepository>();
            var ordenar = app.Setup(o => o.Ordenar(cartas)).Returns(new List<Carta>());
            Assert.AreNotEqual(cartas, ordenar);
        }


        [Test]
        public void OrdenarBaraja()
        {
            List<Carta> cartas = new List<Carta>();
            for (int i = 0; i <= 5; i++) { var tipo = new Random().Next(0, 4); cartas.Add(new Carta() { numero = 1, tipo = tipo }); }
            var app = new Mock<IHomeRepository>();

            var Lista = app.Setup(o => o.Ordenar(cartas)).Returns(new List<Carta>());
            Assert.IsNotNull(Lista);
        }



        [Test]
        public void MostrarManoPoker()
        {
            List<Carta> cartas = new List<Carta>();
            for (int i = 0; i < 4; i++) { cartas.Add(new Carta() { numero = 1, tipo = i }); }

            var app = new Mock<IHomeRepository>();
            var Poker = 1;
            app.Setup(o => o.EstadoJugador(1)).Returns(new List<Jugador>());
            Assert.AreNotEqual(Poker, app.Setup(o => o.Poker(cartas)).Returns(1).ToString());
        }


        [Test]
        public void EstadoJuagdores()
        {
            var app = new Mock<IHomeRepository>();

            app.Setup(o => o.EstadoJugador(1)).Returns(new List<Jugador>());
            Assert.AreNotEqual(app.Setup(o => o.EstadoJugador(1)).Returns(new List<Jugador>()), new List<Jugador>());
        }



        [Test]
        public void JugadorGanador()
        {
            Jugador jugador = new Jugador();
            var app = new Mock<IHomeRepository>();

            var result = app.Setup(o => o.JugadorGanador()).Returns(new List<Jugador>());
            var gano = true;
            Assert.AreNotEqual(jugador.estado, gano);
        }



        [Test]
        public void Perdedor()
        {
            Jugador jugador = new Jugador();
            var app = new Mock<IHomeRepository>();

            var result = app.Setup(o => o.JugadorPerdedor()).Returns(new List<Jugador>());
            var perdio = true;
            Assert.AreNotEqual(jugador.estado, perdio);
        }



        [Test]
        public void Empate()
        {
            Jugador jugador = new Jugador();
            var app = new Mock<IHomeRepository>();

            var result = app.Setup(o => o.JugadoresEmpate(1)).Returns(new List<Jugador>());
            for (int i = 0; i < 4; i++)
            {
                Assert.AreNotEqual(jugador.estado, null);
            }
        }
    }
}